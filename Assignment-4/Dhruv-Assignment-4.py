import numpy as np
import pandas as pd
import random

from pathlib import Path
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
#from mnist import MNIST

# Sigmoid Function
def sigmoid(z):
  s=1/(1+np.exp(-z))
  return s
# Softmax Function  
def softmax(z):
  s=np.exp(z)/np.sum(np.exp(z),axis=0)
  return s
# Initialzing Parameters
# n_x= Number of features
# n_h=Number of hidden layers
# n_c=Number of classes=10  
def init_param(n_x,n_h,n_c):
  params={"w1":np.random.randn(n_h,n_x)*np.sqrt(1/n_x),
          "b1":np.random.randn(n_h,1)*0.01,
          "w2":np.random.randn(n_c,n_h)*np.sqrt(1/n_h),
          "b2":np.random.randn(n_c,1)*0.01
         }
  assert(params["w1"].shape == (n_h, n_x))
  assert(params["b1"].shape == (n_h, 1))
  assert(params["w2"].shape == (n_c, n_h))
  assert(params["b2"].shape == (n_c, 1))
  return params

# Loss Function=-sum(ylogy_hat)/m  
def loss(y,y_hat):
  L=np.sum(np.multiply(y,np.log(y_hat)))
  print(y.shape,y_hat.shape)
  m=y.shape[1]
  L=(-1/m)*L
  
  return L

# Forward Propagation Function  
def forward(x,params):
  cache={}
  #print(x.shape)
  #print()
  cache["z1"]=np.matmul(params["w1"],x)+params["b1"]
  cache["a1"]=sigmoid(cache["z1"])
  cache["z2"]=np.matmul(params["w2"],cache["a1"])+params["b2"]
  cache["a2"]=softmax(cache["z2"])
  return cache

# Backward Propagation Function  
def backward(x,y,params,cache,m_batch):
  
  dz2=cache["a2"]-y
  dw2=(1/m_batch)*np.matmul(dz2,cache["a1"].T)
  db2=(1/m_batch)*np.sum(dz2,axis=1,keepdims=True)
  
  da1=np.matmul(params["w2"].T,dz2)
  dz1=da1*sigmoid(cache["z1"])*(1-sigmoid(cache["z1"]))
  
  dw1=(1./m_batch)*np.matmul(dz1,x.T)
  db1=(1./m_batch)*(np.sum(dz1,axis=1,keepdims=True))
  
  grad={"dw1":dw1, "db1":db1, "dw2":dw2, "db2":db2}
  
  return grad

def main():
  n_h=200
  train=pd.read_csv("/home/dhruv/Downloads/mnist_train.csv")
  
  y_train=np.array(train.iloc[:,0]).reshape(train.iloc[:,0].shape[0],1)
  x_train=(train.iloc[:,1:])
  test=pd.read_csv("/home/dhruv/Downloads/mnist_test.csv")
  y_test=np.array(test.iloc[:,0]).reshape(test.iloc[:,0].shape[0],1)
  x_test=(test.iloc[:,1:])
  
  x=np.vstack((x_train,x_test))
  y=np.vstack((y_train,y_test))
  
  digits=10
  examples=y.shape[0]
  #print(y.shape)
  y=y.reshape(1,examples)
  #print(y.shape)
  y_new=np.eye(digits)[y.astype('int32')]
  y_new=y_new.T.reshape(digits,examples)
  #print(y_new.shape)
  m=60000
  m_test=x.shape[0]-m
  x_train,x_test=x[:m].T,x[m:].T
  y_train,y_test=y_new[:,:m],y_new[:,m:]

  shuffle_index=np.random.permutation(m)
  x_train,y_train=x_train[:,shuffle_index], y_train[:,shuffle_index]
  #print(x_train.shape)
  #print(y_train.shape)
  
  params=init_param(x_train.shape[0],n_h,digits)    
  batch_size=128 
  batches=int(x_train.shape[1]/batch_size)
  alpha=0.01  #Learning Rate
  print(x_train.shape)
  for i in range(10):
      permutation= np.random.permutation(x_train.shape[1])
      x_train_shuffled=x_train[:, permutation]
      y_train_shuffled=y_train[:,permutation]
      
      for j in range(0,batches):
         
          begin=j*batch_size
          end = min(begin + batch_size, x_train.shape[1] - 1)
          X = x_train_shuffled[:, begin:end]
          Y = y_train_shuffled[:, begin:end]
          m_batch = end - begin
          #print(X.shape)
          cache=forward(X,params)
          grads=backward(X,Y,params,cache,m_batch)
          
          params["w1"] = params["w1"] - alpha * grads["dw1"]
          params["b1"] = params["b1"] - alpha * grads["db1"]
          params["w2"] = params["w2"] - alpha * grads["dw2"]
          params["b2"] = params["b2"] - alpha * grads["db2"]
      
      cache=forward(x_train,params)
      train_loss=loss(y_train,cache["a2"])
      
      cache=forward(x_test,params)
      test_loss=loss(y_test,cache["a2"])
      print("Epoch: ",i+1," Training Loss:https://web.whatsapp.com ",train_loss, " Test Loss: ", test_loss)
  cache = forward(x_test, params)
  predictions = np.argmax(cache["a2"], axis=0)
  labels = np.argmax(y_test, axis=0)
  print(classification_report(predictions, labels))    
  print(accuracy_score(predictions,labels)*100)

if __name__=='__main__':
      main()